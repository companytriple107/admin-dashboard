import { useRouter } from 'next/router';
import * as React from 'react';
import Box from '@mui/material/Box';
import Image from 'next/image'
import TextField from '@mui/material/TextField';
import Layout from '../../components/account/Layout.js'
import styles from '../../styles/Home.module.css'
import Logo from '../../public/images/logo.png'

// signup function
export default function signUp() {
    const router = useRouter();
    return (
        <Layout>
            <div className={styles.logo}>
                <Image
                src={Logo}
                alt="logo"
                width={143}
                className={styles.logo}>
                </Image>
            </div>
            <div className={styles.card}>
                <h2>Sign In</h2>
                <div className={styles.underline}></div>
                <Box
                    component="form">
                    <div className={styles.form_style}>
                        <label>Admin Email</label>
                        <TextField required
                            fullWidth
                            id="email"
                            defaultValue="email"
                            className={styles.text_field}>
                            
                        </TextField>
                    </div>
                    <div className={styles.form_style}>
                        <label className={styles.label}>Password</label>
                        <TextField required
                            fullWidth
                            id="password"
                            defaultValue="Password"
                            className={styles.text_field}>
                            
                        </TextField>
                    </div>
                    <div className={styles.form_style}>
                        <button className={styles.submit_btn}>
                            <span>Login</span>
                        </button>
                    </div>
                </Box>
            </div>
        </Layout>
    );
}