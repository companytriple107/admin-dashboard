import { useRouter } from 'next/router';
import styles from '../../styles/Home.module.css'
export default Layout
function Layout({children}) {
    const router = useRouter();
    return (
        <div className={styles.container}>
            <div>
                {children}
            </div>
        </div>
    );
}